# 'compute' is distributed to each node running 'dispynode'
def compute(n):
    import time, socket
    time.sleep(n)
    host = socket.gethostname()
    return (host, n)


def test():
    import dispy, random
    # create a cluster using the application name as secret
    cluster = dispy.JobCluster(compute, secret="dispy_example")
    jobs = []
    
    for i in range(20):
        # schedule execution of 'compute' on a node (running 'dispynode')
        # with a parameter (random number in this case)
        job = cluster.submit(random.randint(5,20))
        job.id = i # optionally associate an ID to job (if needed later)
        jobs.append(job)
    # cluster.wait() # waits for all scheduled jobs to finish
    
    res = [job() for job in jobs]
    cluster.close()

    return res
